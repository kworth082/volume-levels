package com.appsiom.volumelevels

import android.os.Bundle
import android.media.AudioManager
import androidx.appcompat.app.AppCompatActivity
import com.appsiom.volumelevels.ui.main.MainFragment


class MainActivity : AppCompatActivity() {

    fun printVolumes(){
        val audioManager: AudioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        val mediaVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
        val callVolume = audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL)
        val ringVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING)
        val alarmVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM)

        println("Current volume settings\n" + "Media: " + mediaVolume + "\nCall: " + callVolume + "\nRing: " + ringVolume + "\nAlarm: " + alarmVolume)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow()
        }
    }

    override fun onStart() {
        super.onStart()
        printVolumes()
    }
}
