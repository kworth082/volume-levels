package com.appsiom.volumelevels.ui.main

import android.media.AudioManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.appsiom.volumelevels.R

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        // TODO: Use the ViewModel

        view?.let { rootView ->
            val valueMedia = rootView.findViewById<TextView>(R.id.value_media_volume)
            val valueCall = rootView.findViewById<TextView>(R.id.value_call_volume)
            val valueRing = rootView.findViewById<TextView>(R.id.value_ring_volume)
            val valueAlarm = rootView.findViewById<TextView>(R.id.value_alarm_volume)

            val audioManager: AudioManager = activity?.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager

            valueMedia.text = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC).toString()
            valueCall.text = audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL).toString()
            valueRing.text = audioManager.getStreamVolume(AudioManager.STREAM_RING).toString()
            valueAlarm.text = audioManager.getStreamVolume(AudioManager.STREAM_ALARM).toString()
        }
    }

}
